#include "./app.h"
#include <iostream>
#include <rang.hpp>

void print_device(vk::PhysicalDevice dev) {
    using namespace rang;

    auto props = dev.getProperties();
    std::cout << fgB::cyan << "selected device: "                                     //
              << fgB::green << style::bold << props.deviceName << style::reset << " " //
              << fg::green << "(" << vk::to_string(props.deviceType) << ")\n"         //
              << fg::reset;

    std::cout << fgB::cyan << "queues families:\n";
    for (auto&& qf : dev.getQueueFamilyProperties()) {
        std::cout << fgB::green << "  -> queues:" << qf.queueCount << " " //
                  << fg::green << "(" << vk::to_string(qf.queueFlags) << ")\n";
    }

    std::cout << fgB::cyan << "memory:\n";

    auto memory = dev.getMemoryProperties();
    for (uint32_t i = 0; i < memory.memoryHeapCount; i++) {
        std::cout << fgB::cyan << "heap " << i << ": "                                        //
                  << fgB::green << memory.memoryHeaps[i].size << " bytes "                    //
                  << fg::green << "(" << vk::to_string(memory.memoryHeaps[i].flags) << ")\n"; //
    }

    for (uint32_t i = 0; i < memory.memoryTypeCount; i++) {
        std::cout << fgB::cyan << "type " << i << ": "                               //
                  << fgB::green << "heap " << memory.memoryTypes[i].heapIndex << " " //
                  << fg::green << "(" << vk::to_string(memory.memoryTypes[i].propertyFlags) << ")\n";
    }
    std::cout << fg::reset;
}
