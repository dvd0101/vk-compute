#pragma once
#include "../vk/config.h"
#include "./device.h"

namespace vk_compute {
    /**
     * A memory_descr describes the memory requirement need by this application.
     *
     * This application needs two buffers, one for the shader's input and one
     * for the output.
     *
     * A single allocation is performed to obtain enough memory for both the
     * buffers; this means that both buffer points to the same memory, but the
     * second one (the output buffer) starts at an offset equals to the size of
     * the first one.
     */
    struct memory_descr {
        vk::UniqueDeviceMemory mem;
        vk::UniqueBuffer input_buffer;
        vk::UniqueBuffer output_buffer;

        // requested buffer size (in bytes)
        uint64_t input_size;
        uint64_t output_size;

        // size of the allocated memory (in bytes), it can be greater than the
        // sum of the requested size due to paddings and alignments.
        uint64_t allocated_size;
        uint64_t input_allocated_size;
        uint64_t output_allocated_size;
    };

    memory_descr allocate_memory_and_buffers(const device& hw, uint64_t input_size, uint64_t output_size);
}
