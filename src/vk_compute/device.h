#pragma once
#include "../vk/config.h"

namespace vk_compute {
    /**
     * A device needed by this application.
     *
     * It keeps under the same struct both the phsyical and logical devices and
     * the queues families selected for submit compute and transfer jobs.
     *
     * The device is configured to open at most one queue for each family.
     */
    struct device {
        constexpr static int compute_queues = 1;
        constexpr static int transfer_queues = 1;

        vk::PhysicalDevice phys;
        vk::UniqueDevice dev;
        // XXX remove queue
        vk::Queue compute;
        vk::Queue transfer;
        uint32_t compute_family;
        uint32_t transfer_family;
    };

    /**
     * Creates a vulkan device (from the given physical one) configured
     * according to the application needs.
     *
     * The device has access to:
     * - one compute queue
     * - one transfer queue
     */
    device create_device(vk::PhysicalDevice);
}
