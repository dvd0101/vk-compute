#include "./memory.h"
#include "../phys/detect.h"

namespace {
    std::pair<vk::UniqueBuffer, vk::UniqueBuffer> create_buffers(vk::Device dev,
                                                                 uint64_t input_size,
                                                                 uint64_t output_size) {
        auto input_buffer = dev.createBufferUnique({
            .size = input_size,
            .usage = vk::BufferUsageFlagBits::eStorageBuffer,
            .sharingMode = vk::SharingMode::eExclusive,
        });

        auto output_buffer = dev.createBufferUnique({
            .size = output_size,
            .usage = vk::BufferUsageFlagBits::eStorageBuffer,
            .sharingMode = vk::SharingMode::eExclusive,
        });

        return {std::move(input_buffer), std::move(output_buffer)};
    }

    std::pair<vk::UniqueDeviceMemory, std::array<uint64_t, 2>> allocate_memory_for_buffers(
        vk::PhysicalDevice phys,
        vk::Device dev,
        std::array<vk::Buffer, 2> buffers) {

        uint32_t memory_type = 0;
        uint64_t total_size = 0;
        std::array<uint64_t, 2> buffer_sizes;

        for (size_t i = 0; i < buffers.size(); i++) {
            auto req = dev.getBufferMemoryRequirements(buffers[i]);
            auto buff_memory = phys::find_suitable_memory(phys, req);

            if (!buff_memory) {
                throw resource_unavailable("needed memory type not found");
            }

            if (i > 0 && memory_type != *buff_memory)
                throw resource_unavailable("memory type mismatch between buffers");

            memory_type = *buff_memory;
            buffer_sizes[i] = req.size;
            total_size += req.size;
        }

        auto mem = dev.allocateMemoryUnique({
            .allocationSize = total_size,
            .memoryTypeIndex = memory_type,
        });

        return {std::move(mem), buffer_sizes};
    }
}

namespace vk_compute {
    memory_descr allocate_memory_and_buffers(const device& hw, uint64_t input_size, uint64_t output_size) {
        auto [input_buffer, output_buffer] = create_buffers(*hw.dev, input_size, output_size);
        auto [mem, sizes] = allocate_memory_for_buffers(hw.phys, *hw.dev, {*input_buffer, *output_buffer});

        hw.dev->bindBufferMemory(*input_buffer, *mem, 0);
        hw.dev->bindBufferMemory(*output_buffer, *mem, sizes[0]);
        return {
            .mem = std::move(mem),
            .input_buffer = std::move(input_buffer),
            .output_buffer = std::move(output_buffer),
            .input_size = input_size,
            .output_size = output_size,
            .allocated_size = sizes[0] + sizes[1],
            .input_allocated_size = sizes[0],
            .output_allocated_size = sizes[1],
        };
    }
}
