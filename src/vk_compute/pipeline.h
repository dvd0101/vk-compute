#pragma once
#include "../vk/config.h"

namespace vk_compute {
    /**
     * A pipeline_descr describes the pipeline needed by this application:
     *
     * The contained pipeline is a "compute pipeline" and it executes the
     * "main" function of a shader.
     *
     * The pipeline layout contains *one* set of descriptors (`dset_layout`)
     * with two bindings: 0 and 1.
     *
     * The bindings are configured to be connected to a shader storage buffer
     * (ssbo).
     *
     * A shader compatible with this pipeline looks like:
     * ```
     * layout(std430, binding = 0) restrict readonly buffer InputBuffer {
     *     ...
     * } src_data;
     *
     * layout(std430, binding = 1) restrict writeonly buffer OutputBuffer {
     *     ...
     * } dst_data;
     * ```
     *
     * This application uses the first buffer (binding 0) as the input data and
     * the second buffer (binding 1) as the output data.
     */
    struct pipeline_descr {
        vk::UniquePipeline pipeline;
        vk::UniquePipelineLayout layout;
        vk::UniqueDescriptorSetLayout dset_layout;
    };

    // construct a compute pipeline reading the shader bytecode from the specified file
    pipeline_descr create_compute_pipeline(vk::Device dev, const std::string& shader_file);
}
