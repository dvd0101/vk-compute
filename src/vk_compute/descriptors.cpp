#include "./descriptors.h"

namespace vk_compute {
    descriptorset_descr connect_buffers_to_pipeline(vk::Device dev, const pipeline_descr& pv, const memory_descr& ml) {
        vk::DescriptorPoolSize pool_size{
            .type = vk::DescriptorType::eStorageBuffer,
            .descriptorCount = 2,
        };

        auto pool = dev.createDescriptorPoolUnique({
            .flags = vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet,
            .maxSets = 1,
            .poolSizeCount = 1,
            .pPoolSizes = &pool_size,
        });

        auto dsets = dev.allocateDescriptorSetsUnique({
            .descriptorPool = *pool,
            .descriptorSetCount = 1,
            .pSetLayouts = &(*pv.dset_layout),
        });

        auto dset = std::move(dsets[0]);

        vk::DescriptorBufferInfo in_info{
            .buffer = *ml.input_buffer,
            .offset = 0,
            .range = ml.input_size,
        };

        vk::DescriptorBufferInfo out_info{
            .buffer = *ml.output_buffer,
            .offset = 0,
            .range = ml.output_size,
        };

        std::array<vk::WriteDescriptorSet, 2> write_ops{
            vk::WriteDescriptorSet{.dstSet = *dset,
                                   .dstBinding = 0,
                                   .descriptorCount = 1,
                                   .descriptorType = vk::DescriptorType::eStorageBuffer,
                                   .pBufferInfo = &in_info},
            vk::WriteDescriptorSet{.dstSet = *dset,
                                   .dstBinding = 1,
                                   .descriptorCount = 1,
                                   .descriptorType = vk::DescriptorType::eStorageBuffer,
                                   .pBufferInfo = &out_info},
        };

        dev.updateDescriptorSets(write_ops, {});

        return {
            .pool = std::move(pool),
            .set0 = std::move(dset),
        };
    }
}
