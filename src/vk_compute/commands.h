#pragma once
#include "../vk/config.h"
#include "./pipeline.h"

namespace vk_compute {
    /**
     * A command_descr describes the commands needed to executs the application
     * pipelines binded to the input and output buffers
     *
     * This struct contains both the pool and one command buffer (the only one
     * needed by this application) allocated from the pool itself.
     */
    struct command_descr {
        vk::UniqueCommandPool pool;
        vk::UniqueCommandBuffer cmd_buffer;
    };

    command_descr allocate_and_record_commands(vk::Device dev,
                                               uint32_t compute_queue_family,
                                               const pipeline_descr& pv,
                                               vk::DescriptorSet dset);
}
