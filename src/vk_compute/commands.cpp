#include "./commands.h"

namespace vk_compute {
    command_descr allocate_and_record_commands(vk::Device dev,
                                               uint32_t compute_queue_family,
                                               const pipeline_descr& pv,
                                               vk::DescriptorSet dset) {
        auto cmd_pool = dev.createCommandPoolUnique({
            .queueFamilyIndex = compute_queue_family,
        });

        auto buffers = dev.allocateCommandBuffersUnique({
            .commandPool = *cmd_pool,
            .level = vk::CommandBufferLevel::ePrimary,
            .commandBufferCount = 1,
        });

        auto buffer = std::move(buffers[0]);
        buffer->begin({.flags = {}});

        buffer->bindPipeline(vk::PipelineBindPoint::eCompute, *pv.pipeline);
        buffer->bindDescriptorSets(vk::PipelineBindPoint::eCompute, *pv.layout, 0, {dset}, {});

        /* buffer->pushConstants(pipeLayout, vk::ShaderStageFlagBits::eCompute, 0, p); */

        // Start the compute pipeline, and execute the compute shader.
        // The number of workgroups is specified in the arguments.
        buffer->dispatch(1, 1, 1); // div_up(p.width, WORKGROUP_SIZE), div_up(p.height, WORKGROUP_SIZE), 1);

        buffer->end();

        return {
            .pool = std::move(cmd_pool),
            .cmd_buffer = std::move(buffer),
        };
    }
}
