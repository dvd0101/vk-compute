#pragma once
#include "../vk/config.h"
#include "./memory.h"
#include "./pipeline.h"

namespace vk_compute {
    /**
     * A descriptorset_descr describes the descriptor set used by this
     * application to connect the input and output buffers to the pipeline.
     *
     * This struct contains both the pool and one descriptor set (the only one
     * needed by this application) allocated from the pool itself.
     */
    struct descriptorset_descr {
        vk::UniqueDescriptorPool pool;
        vk::UniqueDescriptorSet set0;
    };

    descriptorset_descr connect_buffers_to_pipeline(vk::Device dev, const pipeline_descr& pv, const memory_descr& ml);
}
