#include "../phys/detect.h"
#include "./device.h"

namespace vk_compute {
    device create_device(vk::PhysicalDevice phys) {
        auto qf = phys::find_queue_families(phys);
        const uint32_t compute_family = *qf.compute_index;
        const uint32_t transfer_family = *qf.transfer_index;
        const float queue_priority = 1;

        std::vector<vk::DeviceQueueCreateInfo> queues = {{
            .queueFamilyIndex = compute_family,
            .queueCount = 1,
            .pQueuePriorities = &queue_priority,
        }};

        // if is the same queue family that is used for both compute and
        // trasnferit must be requested only once
        if (transfer_family != compute_family) {
            queues.push_back({
                .queueFamilyIndex = transfer_family,
                .queueCount = 1,
                .pQueuePriorities = &queue_priority,
            });
        }

        auto dev = phys.createDeviceUnique({
            .queueCreateInfoCount = static_cast<uint32_t>(queues.size()),
            .pQueueCreateInfos = queues.data(),
        });

        device result;

        result.phys = phys;
        result.compute = dev->getQueue(compute_family, 0);
        result.compute_family = compute_family;
        result.transfer_family = transfer_family;
        if (transfer_family != compute_family) {
            result.transfer = dev->getQueue(transfer_family, 0);
        } else {
            result.transfer = result.compute;
        }
        result.dev = std::move(dev);

        return result;
    }
}
