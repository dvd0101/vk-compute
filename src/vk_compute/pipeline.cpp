#include "./pipeline.h"
#include <fstream>

namespace {
    std::vector<char> read_file(const std::string& fname) {
        std::ifstream in(fname, std::ios::ate | std::ios::binary);
        if (!in)
            throw resource_unavailable("cannot open the requested file");

        std::vector<char> output(in.tellg());
        in.seekg(0);
        in.read(output.data(), output.size());

        return output;
    }

    vk::UniqueShaderModule load_shader(vk::Device dev, const std::string& file_name) {
        auto bytecode = read_file(file_name);
        return dev.createShaderModuleUnique({
            .codeSize = bytecode.size(),
            .pCode = reinterpret_cast<const uint32_t*>(bytecode.data()),
        });
    }

}

namespace vk_compute {
    pipeline_descr create_compute_pipeline(vk::Device dev, const std::string& shader_file) {
        auto shader = load_shader(dev, shader_file);

        const vk::DescriptorSetLayoutBinding bindings[] = {{
                                                               .binding = 0,
                                                               .descriptorType = vk::DescriptorType::eStorageBuffer,
                                                               .descriptorCount = 1,
                                                               .stageFlags = vk::ShaderStageFlagBits::eCompute,
                                                           },
                                                           {
                                                               .binding = 1,
                                                               .descriptorType = vk::DescriptorType::eStorageBuffer,
                                                               .descriptorCount = 1,
                                                               .stageFlags = vk::ShaderStageFlagBits::eCompute,
                                                           }};

        auto descriptors = dev.createDescriptorSetLayoutUnique({
            .bindingCount = 2,
            .pBindings = bindings,
        });

        auto pipeline_layout = dev.createPipelineLayoutUnique({
            .setLayoutCount = 1,
            .pSetLayouts = &(*descriptors),
        });

        vk::PipelineShaderStageCreateInfo shinfo{
            .stage = vk::ShaderStageFlagBits::eCompute,
            .module = *shader,
            .pName = "main",
        };

        auto r = dev.createComputePipelineUnique(nullptr,
                                                 {
                                                     .stage = shinfo,
                                                     .layout = *pipeline_layout,
                                                 });

        if (r.result != vk::Result::eSuccess) {
            throw resource_unavailable{"cannot create the comput pipeline: " + vk::to_string(r.result)};
        }
        return {
            .pipeline = std::move(r.value),
            .layout = std::move(pipeline_layout),
            .dset_layout = std::move(descriptors),
        };
    }
}
