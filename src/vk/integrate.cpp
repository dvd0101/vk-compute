#include "./config.h"

// see: https://github.com/KhronosGroup/Vulkan-Hpp#extensions--per-device-function-pointers
VULKAN_HPP_DEFAULT_DISPATCH_LOADER_DYNAMIC_STORAGE

void load_global_functions() {
    static vk::DynamicLoader dl;
    PFN_vkGetInstanceProcAddr vkGetInstanceProcAddr =
        dl.getProcAddress<PFN_vkGetInstanceProcAddr>("vkGetInstanceProcAddr");
    VULKAN_HPP_DEFAULT_DISPATCHER.init(vkGetInstanceProcAddr);
}

void load_instance_functions(vk::Instance instance) { VULKAN_HPP_DEFAULT_DISPATCHER.init(instance); }

void load_device_functions(vk::Device dev) { VULKAN_HPP_DEFAULT_DISPATCHER.init(dev); }
