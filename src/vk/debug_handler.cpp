#include "./config.h"
#include <iostream>
#include <rang.hpp>

namespace {
    VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT message_severity,
                                                 VkDebugUtilsMessageTypeFlagsEXT message_type,
                                                 const VkDebugUtilsMessengerCallbackDataEXT* data,
                                                 void*) {

        using namespace rang;

        if (message_severity >= (VkDebugUtilsMessageTypeFlagsEXT)vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning) {
            std::cerr << fgB::red;
        } else {
            std::cerr << fgB::yellow;
        }

        std::cerr << vk::to_string(static_cast<vk::DebugUtilsMessageSeverityFlagBitsEXT>(message_severity)) << ": "
                  << vk::to_string(static_cast<vk::DebugUtilsMessageTypeFlagsEXT>(message_type)) << ":\n";
        std::cerr << fg::reset;

        std::cerr << "\t"                                                           //
                  << "message id   = <" << data->pMessageIdName << "> "             //
                  << "(" << std::hex << data->messageIdNumber << std::dec << ")\n"; //
        std::cerr << "\t"
                  << "message      = <" << fgB::yellow << data->pMessage << fg::reset << ">\n";

        if (0 < data->queueLabelCount) {
            std::cerr << "\t"
                      << "Queue Labels:\n";
            for (uint8_t i = 0; i < data->queueLabelCount; i++) {
                std::cerr << "\t\t"
                          << "labelName = <" << data->pQueueLabels[i].pLabelName << ">\n";
            }
        }
        if (0 < data->cmdBufLabelCount) {
            std::cerr << "\t"
                      << "CommandBuffer Labels:\n";
            for (uint8_t i = 0; i < data->cmdBufLabelCount; i++) {
                std::cerr << "\t\t"
                          << "labelName = <" << data->pCmdBufLabels[i].pLabelName << ">\n";
            }
        }
        if (0 < data->objectCount) {
            std::cerr << "\t"
                      << "Objects:\n";
            for (uint8_t i = 0; i < data->objectCount; i++) {
                std::cerr << "\t\t"
                          << "Object " << i << "\n";
                std::cerr << "\t\t\t"
                          << "objectType   = "
                          << vk::to_string(static_cast<vk::ObjectType>(data->pObjects[i].objectType)) << "\n";
                std::cerr << "\t\t\t"
                          << "objectHandle = " << data->pObjects[i].objectHandle << "\n";
                if (data->pObjects[i].pObjectName) {
                    std::cerr << "\t\t\t"
                              << "objectName   = <" << data->pObjects[i].pObjectName << ">\n";
                }
            }
        }

        std::cerr << fg::reset;

        return VK_FALSE;
    }
}

vk::UniqueDebugUtilsMessengerEXT install_debug_handler(vk::Instance instance) {
    vk::DebugUtilsMessengerCreateInfoEXT debug_info{
        .messageSeverity = vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning //
                           | vk::DebugUtilsMessageSeverityFlagBitsEXT::eError //
        // | vk::DebugUtilsMessageSeverityFlagBitsEXT::eVerbose
        // | vk::DebugUtilsMessageSeverityFlagBitsEXT::eInfo
        ,
        .messageType = vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneral
                       | vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformance
                       | vk::DebugUtilsMessageTypeFlagBitsEXT::eValidation,
        .pfnUserCallback = debugCallback,
    };

    return instance.createDebugUtilsMessengerEXTUnique(debug_info);
}
