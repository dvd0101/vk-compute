#pragma once
/**
 * This header describes the way the application integrates with vulkan;
 * it includes and configures the vulkan SDK, and exposes some utility
 * functions and types.
 *
 * For this application I decided to:
 * - disable the struct constructors (VULKAN_HPP_NO_STRUCT_CONSTRUCTORS) to
 *   take advantage of the designated initializers (c++20)
 * - use the dynamic loader shipped with vulkan-hpp
 *   (VULKAN_HPP_DISPATCH_LOADER_DYNAMIC)
 */

#define VULKAN_HPP_NO_STRUCT_CONSTRUCTORS
#define VULKAN_HPP_DISPATCH_LOADER_DYNAMIC 1
#include <vulkan/vulkan.hpp>

// raised whenever a vk-resource (device, memory and so on) is not available
class resource_unavailable : public std::runtime_error {
  public:
    using std::runtime_error::runtime_error;
};

// Configures the dynamic loader with global, instance and device functions
//
// see: https://github.com/KhronosGroup/Vulkan-Hpp#extensions--per-device-function-pointers
void load_global_functions();
void load_instance_functions(vk::Instance);
void load_device_functions(vk::Device);

// install a custom debug handler, the vulkan instance must have the validation
// layer: VK_LAYER_KHRONOS_validation
vk::UniqueDebugUtilsMessengerEXT install_debug_handler(vk::Instance);
