#include "./app.h"
#include "./phys/detect.h"
#include "./time_it.h"
#include <iostream>

vk::UniqueInstance create_instance() {
    vk::ApplicationInfo app{
        .pApplicationName = "Compute test",
        .applicationVersion = VK_MAKE_VERSION(0, 0, 1),
        .apiVersion = VK_API_VERSION_1_0,
    };

    const char* required_extensions[] = {VK_EXT_DEBUG_UTILS_EXTENSION_NAME};
    const char* layers[] = {"VK_LAYER_KHRONOS_validation"};

    vk::InstanceCreateInfo info{
        .pApplicationInfo = &app,
        .enabledLayerCount = 1,
        .ppEnabledLayerNames = layers,
        .enabledExtensionCount = 1,
        .ppEnabledExtensionNames = required_extensions,
    };

    return vk::createInstanceUnique(info);
}

vk_compute::device create_device(vk::Instance instance) {
    auto detect = phys::find_physical_device(instance);
    if (!detect)
        throw resource_unavailable("suitable hw device not found");

    return vk_compute::create_device(*detect);
}

int main() {
    load_global_functions();

    vk::UniqueInstance instance = create_instance();
    load_instance_functions(*instance);

    vk::UniqueDebugUtilsMessengerEXT dbg = install_debug_handler(*instance);

    try {

        vk_compute::device hw = create_device(*instance);

        print_device(hw.phys);

        time_it("do work...", [&] { start_work(hw); });

    } catch (const resource_unavailable& e) {
        std::cerr << "resource unavailable: " << e.what() << "\n";
        std::exit(2);
    }
}
