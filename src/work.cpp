#include "./app.h"
#include "./phys/detect.h"
#include "./time_it.h"
#include "./vk_compute/commands.h"
#include "./vk_compute/descriptors.h"
#include "./vk_compute/memory.h"
#include "./vk_compute/pipeline.h"
#include <iostream>

void start_work(const vk_compute::device& hw) {
    auto compute_pipeline = time_it("  assemble pipeline...", //
                                    [&] { return vk_compute::create_compute_pipeline(*hw.dev, "./comp.spv"); });

    auto memory = time_it("  allocate memory...", //
                          [&] { return allocate_memory_and_buffers(hw, 400, 400); });

    auto dset = time_it("  connect buffers...", //
                        [&] { return connect_buffers_to_pipeline(*hw.dev, compute_pipeline, memory); });

    auto commands =
        time_it("  record commands...",                                                                            //
                [&] {                                                                                              //
                    return allocate_and_record_commands(*hw.dev, hw.compute_family, compute_pipeline, *dset.set0); //
                });

    time_it("  copy memory to card...", [&] {
        auto ptr = (float*)hw.dev->mapMemory(*memory.mem, 0, memory.input_allocated_size);
        for (size_t i = 0; i < memory.input_allocated_size / sizeof(float); i++)
            ptr[i] = -123;
        hw.dev->unmapMemory(*memory.mem);
    });

    auto fence = hw.dev->createFenceUnique({});
    time_it("  submit commands...", [&] {
        vk::SubmitInfo info{
            .commandBufferCount = 1,
            .pCommandBuffers = &(*commands.cmd_buffer),
        };
        hw.compute.submit({info}, *fence);
    });

    auto result = time_it("  waiting...", [&] { return hw.dev->waitForFences({*fence}, true, 1e10); });

    if (result == vk::Result::eTimeout) {
        std::cout << "timeout!\n";
        return;
    } else if (result != vk::Result::eSuccess) {
        std::cout << "fail!\n";
        return;
    }
    std::cout << "done\n";

    time_it("  copy memory from card...", [&] {
        auto ptr = (float*)hw.dev->mapMemory(*memory.mem, memory.input_allocated_size, memory.output_allocated_size);
        std::cout << ptr[0] << "\n";
        std::cout << ptr[1] << "\n";
        std::cout << ptr[2] << "\n";
        std::cout << ptr[3] << "\n";
        std::cout << ptr[4] << "\n";
        std::cout << ptr[5] << "\n";
        std::cout << ptr[6] << "\n";
        std::cout << ptr[7] << "\n";
        hw.dev->unmapMemory(*memory.mem);
    });
}
