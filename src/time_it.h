#pragma once
#include <chrono>
#include <exception>
#include <iostream>
#include <string_view>
#include <utility>

template <typename Func>
struct scoped_exit {
    const Func f;

    constexpr explicit scoped_exit(Func&& f) noexcept : f(std::forward<Func>(f)) {}

    ~scoped_exit() noexcept(std::is_nothrow_invocable_v<Func>) {
        // Run cleanup only when there are no uncaught_exceptions
        if (std::uncaught_exceptions() == 0) [[likely]]
            f();
    }

    scoped_exit(const scoped_exit&) = delete;
    scoped_exit(scoped_exit&&) = delete;
    scoped_exit& operator=(scoped_exit&&) = delete;
    scoped_exit& operator=(const scoped_exit&) = delete;
};

template <typename Func>
auto time_it(std::string_view msg, Func&& fn) {
    using namespace std::chrono;
    auto t0 = high_resolution_clock::now();

    scoped_exit guard([&]() {
        auto t1 = high_resolution_clock::now();
        std::cout << msg << " " << duration_cast<microseconds>(t1 - t0).count() << "µs\n";
    });

    return fn();
}
