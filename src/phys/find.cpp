#include "./detect.h"

namespace phys {
    queue_families find_queue_families(vk::PhysicalDevice dev) {
        queue_families qf;

        auto queues = dev.getQueueFamilyProperties();
        for (uint32_t i = 0; i < queues.size(); i++) {
            if (queues[i].queueFlags & vk::QueueFlagBits::eCompute) {
                qf.compute_index = i;
            }
            if (queues[i].queueFlags & vk::QueueFlagBits::eTransfer) {
                qf.transfer_index = i;
            }
        }
        return qf;
    }

    std::optional<vk::PhysicalDevice> find_physical_device(vk::Instance instance) {
        auto is_valid_kind = [](vk::PhysicalDevice dev) {
            auto props = dev.getProperties();
            return (props.deviceType == vk::PhysicalDeviceType::eDiscreteGpu
                    || props.deviceType == vk::PhysicalDeviceType::eIntegratedGpu);
        };

        auto has_correct_queues = [](vk::PhysicalDevice dev) {
            auto qf = find_queue_families(dev);
            return qf.compute_index.has_value() && qf.transfer_index.has_value();
        };

        for (auto dev : instance.enumeratePhysicalDevices()) {
            if (is_valid_kind(dev) && has_correct_queues(dev))
                return dev;
        }
        return {};
    }

    std::optional<uint32_t> find_suitable_memory(vk::PhysicalDevice dev, vk::MemoryRequirements req) {
        vk::MemoryPropertyFlags required_properties =
            vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent;

        auto memory = dev.getMemoryProperties();
        for (uint32_t i = 0; i < memory.memoryTypeCount; i++) {
            auto& props = memory.memoryTypes[i].propertyFlags;
            if (req.memoryTypeBits & (1 << i) && (props & required_properties)) {
                return i;
            }
        }

        return {};
    }
}
