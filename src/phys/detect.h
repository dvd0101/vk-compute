#pragma once
/**
 * This header contains functions and types related with the "phsyical device",
 * mainly used to inspect the capabilities of a physical device.
 */

#include "../vk/config.h"
#include <optional>

namespace phys {
    struct queue_families {
        std::optional<uint32_t> compute_index;
        std::optional<uint32_t> transfer_index;
    };

    /**
     * search for a PhysicalDevice with the needed characteristics.
     *
     * It returns the best *discrete* or *integrated* GPU wich has both a
     * compute and transfer queue family.
     *
     * Note: ATM best means first
     */
    std::optional<vk::PhysicalDevice> find_physical_device(vk::Instance);

    /**
     * Inspect the device and returns the best compute and transfer queue
     * family.
     *
     * Note: ATM best means first
     */
    queue_families find_queue_families(vk::PhysicalDevice);

    /**
     * search for the index of the device's memory type that fullfills the
     * memory requirements
     */
    std::optional<uint32_t> find_suitable_memory(vk::PhysicalDevice, vk::MemoryRequirements);
}
