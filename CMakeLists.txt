cmake_minimum_required(VERSION 3.14)

project(vktest VERSION 0.0.1 LANGUAGES CXX)

include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup(TARGETS)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

add_executable(
    vktest
    src/main.cpp
    src/vk/integrate.cpp
    src/vk/debug_handler.cpp
    src/phys/find.cpp
    src/vk_compute/create_device.cpp
    src/vk_compute/pipeline.cpp
    src/vk_compute/memory.cpp
    src/vk_compute/descriptors.cpp
    src/vk_compute/commands.cpp
    src/print_device.cpp
    src/work.cpp
)
target_compile_options(vktest PRIVATE -Wall -Wextra)
target_link_libraries(vktest PRIVATE vulkan CONAN_PKG::rang)
target_compile_features(vktest PRIVATE cxx_std_20)
